package net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCrudRestfulWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCrudRestfulWebserviceApplication.class, args);
	}
}
