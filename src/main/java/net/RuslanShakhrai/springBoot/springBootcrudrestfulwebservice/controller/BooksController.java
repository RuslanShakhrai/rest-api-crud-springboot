package net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.controller;

import net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.exception.ResourceNotFoundException;
import net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.model.Books;
import net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BooksController {
    @Autowired
    BooksService booksService;
    //create a get mapping that retrieves all book detail from database
    @GetMapping("/books")
    private List<Books> getAllBooks(){
        return booksService.getAllBooks();
    }
    @GetMapping("/books/{id}")
    private ResponseEntity<Books> getBook(@PathVariable("id") int id) throws ResourceNotFoundException  {
         Books book = booksService.getBookByid(id);
         return ResponseEntity.ok().body(book);
    }
    @DeleteMapping("/books/{id}")
    private void deleteBook(@PathVariable("id") int id) throws ResourceNotFoundException {
        booksService.delete(id);
    }
    @PostMapping("/books")
    private ResponseEntity<Books> saveBook(@RequestBody Books book){
        booksService.saveOrUpdate(book);
        return ResponseEntity.ok().build();
    }
    @PutMapping("/books/{id}")
    private ResponseEntity<Books> updateBook(@PathVariable("id") int id,@RequestBody Books book) throws ResourceNotFoundException {
        Books prevBook =  booksService.getBookByid(id);
        prevBook.setTitle(book.getTitle());
        prevBook.setAuthor(book.getAuthor());
        prevBook.setYear(book.getYear());
        prevBook.setPages(book.getPages());
        booksService.saveOrUpdate(prevBook);
        return ResponseEntity.ok(prevBook);
    }
}
