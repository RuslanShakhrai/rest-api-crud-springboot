package net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.repository;

import net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.model.Books;
import org.springframework.data.repository.CrudRepository;

public interface BooksRepository extends CrudRepository<Books,Integer> {
    
}
