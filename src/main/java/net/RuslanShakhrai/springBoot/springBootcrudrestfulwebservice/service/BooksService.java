package net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.service;

import net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.exception.ResourceNotFoundException;
import net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.model.Books;
import net.RuslanShakhrai.springBoot.springBootcrudrestfulwebservice.repository.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//logic
@Service
public class BooksService {
    @Autowired
    BooksRepository booksRepository;
    //get all books records by using findall of CrudRepository
    public List<Books> getAllBooks(){
        List<Books> books = new ArrayList<>();
        booksRepository.findAll().forEach(books1->books.add(books1));
        return books;
    }
    //get specific record by using method findByID() of CrudRepository
    public Books getBookByid(int id) throws ResourceNotFoundException {
        return booksRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Book not found for this id::"+id));
    }
    //saving specific record by using method save() of CrudRepository
    public void saveOrUpdate(Books book){
        booksRepository.save(book);
    }
    //deleting a specific record by using the method deleteById of CrudRepository
    public void delete(int id) throws ResourceNotFoundException{
        Books book = getBookByid(id);
        booksRepository.deleteById(book.getId());
    }
}
